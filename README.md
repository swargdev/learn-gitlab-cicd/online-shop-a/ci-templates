# Used Variables

Add this into your ci-file

```yml
variables:
  MICRO_SERVICE: name
  SERVICE_VERSION: "1.0"
  SERVICE_PORT: 8000
```

# Variables used to deploy to dev-server:

- DEV_SERVER_HOST
- DEV_SERVER_USER
- DEV_ENDPOINT

You can define this via GitLab Ui at Group->Settings->CICD->Variables
